import axiosApi from '../../axiosApi';

export const FETCH_TASKS_SUCCESS = 'FETCH_TASKS_SUCCESS';

export const fetchTasksSuccess = tasks => {
    return {type: FETCH_TASKS_SUCCESS, tasks}
};

export const fetchShops = () => {
    return async dispatch => {
        try {
            const response = await axiosApi.get('/tasks');
            dispatch(fetchTasksSuccess(response.data));
        } catch(e) {
            console.error(e);
        }
    }
};