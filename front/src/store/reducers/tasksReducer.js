const { FETCH_TASKS_SUCCESS } = require("../actions/tasksActions");

const initialState = {
    tasks: []
};

const tasksReducer = (state = initialState, action) => {
    switch(action.type) {
        case FETCH_TASKS_SUCCESS:
            return {...state, tasks: action.tasks};
        default:
            return {...state}
    }
};

export default tasksReducer;