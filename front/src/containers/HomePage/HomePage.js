import React from 'react';
import { useDispatch, useSelector } from 'react-redux';

const HomePage = () => {
    const dispatch = useDispatch();
    const tasks = useSelector(state => state.tasks.tasks);
    return (
        <>
            {tasks && tasks.map((task,index) => {
                return (
                    <div key={index}>{task}</div>
                )
            })}
        </>
    )
};

export default HomePage;