import { Route, Switch } from 'react-router-dom';
import './App.css';
import HomePage from './containers/HomePage/HomePage';

function App() {
  return (
    <div className="App">
      <main>
        <Switch>
          <Route path='/' exact component={HomePage}/>
        </Switch>
      </main>
    </div>
  );
}

export default App;
