const mongoose = require('mongoose');
const {nanoid} = require('nanoid');
const config = require('./app/config');
const Shop = require('./app/models/Shop');

mongoose.connect(config.db.url + '/' + config.db.name, {useNewUrlParser: true, useUnifiedTopology: true});
const db = mongoose.connection;
db.once('open', async() => {
    try {
        await db.dropCollection('shops');
    } catch (e) {
        console.log('Collection were not present, skipping drop...');
    }

    const [shop1, shop2] = await Shop.create({
        name: 'Pacacina',
        description: 'Отличная грузинская кухня',
        photo: ''
    }, {
        name: 'KFC',
        description: 'Острые крылышки',
        photo: ''
    })
    await db.close();
})